package co.clund.javacms.ldap;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.directory.api.ldap.model.cursor.EntryCursor;
import org.apache.directory.api.ldap.model.entry.Attribute;
import org.apache.directory.api.ldap.model.entry.Entry;
import org.apache.directory.api.ldap.model.entry.Value;
import org.apache.directory.api.ldap.model.exception.LdapException;
import org.apache.directory.api.ldap.model.message.SearchScope;
import org.apache.directory.ldap.client.api.LdapConnection;
import org.apache.directory.ldap.client.api.LdapNetworkConnection;
import org.apache.directory.ldap.client.api.search.FilterBuilder;
import org.json.JSONObject;

import static org.apache.directory.ldap.client.api.search.FilterBuilder.and;
import static org.apache.directory.ldap.client.api.search.FilterBuilder.equal;

public class LdapConnector {

    private static final String LDAP_ATTR_JPG_PHOTO = "jpegphoto";

    private final String connectionHost;
    private final int connectionPort;
    private final boolean connectionSSL;
    private final String connectionDn;
    private final String connectionPwd;

    public LdapConnector(JSONObject config) {
        connectionHost = config.getString("host");
        connectionPort = config.getInt("port");
        connectionSSL = config.getBoolean("ssl");
        connectionDn = config.getString("dn");
        connectionPwd = config.getString("password");
    }

    protected LdapConnection getLdapConnection() {
        return new LdapNetworkConnection(connectionHost, connectionPort, connectionSSL);
    }

    protected LdapConnection getBoundLdapConnection() throws LdapException {
        LdapConnection ldapCon = getLdapConnection();
        ldapCon.bind(connectionDn, connectionPwd);
        return ldapCon;
    }

    private List<Map<String, List<String>>> searchMultiInsecure(String baseDn, String filter,
            SearchScope scope, String... attributes) throws IOException, LdapException {
        List<Map<String, List<String>>> retList = new ArrayList<>();

        try (LdapConnection ldapCon = getBoundLdapConnection()) {
            /*System.out.println("search:\n"
                    + "   baseDn: " + baseDn + "\n"
                    + "   filter: " + filter + "\n"
                    + "   scope: " + scope + "\n"
                    + "   attributes: " + String.join(",", attributes) + "\n");*/
            try (EntryCursor cursor = ldapCon.search(baseDn, filter, scope, attributes)) {
                for (Entry entry : cursor) {
                    Collection<Attribute> attrs = entry.getAttributes();
                    Map<String, List<String>> thisMap = new HashMap<>();

                    //System.out.println("result:");

                    for (Attribute a : attrs) {
                        //System.out.print("   " + a.getId() + " : ");
                        for (Value v : a) {
                            if (!thisMap.containsKey(a.getId())) {
                                thisMap.put(a.getId(), new ArrayList<String>());
                            }

                            if (a.getId().equals(LDAP_ATTR_JPG_PHOTO)) {
                                thisMap.get(a.getId()).add(Base64.encodeBase64String(v.getBytes()));
                            } else {
                                thisMap.get(a.getId()).add(v.getString());
                                //System.out.print(v.getString() + ",");
                            }
                        }
                        //System.out.println();
                    }
                    retList.add(thisMap);
                }
            }
        }
        return retList;
    }

    public List<Map<String, List<String>>> searchChildrenMulti(String baseDn, String classname,
            String selectorAttr, String selectorVal, String... attributes)
            throws IOException, LdapException {
        if (!baseDn.matches("[a-zA-Z0-9,= -_]+")) {
            throw new RuntimeException("baseDn \"" + baseDn + "\" is not a valid distinguished name path!");
        }
        if (!classname.matches("[a-zA-Z0-9]+")) {
            throw new RuntimeException("classname \"" + classname + "\" is not a valid class name!");
        }
        if (selectorAttr != null && !selectorAttr.matches("[a-zA-Z0-9]+")) {
            throw new RuntimeException("selectorAttr \"" + selectorAttr + "\" is not a valid attribute name!");
        }

        for (String s : attributes) {
            if (!s.matches("[a-zA-Z0-9]+")) {
                throw new RuntimeException(
                        "requested attribute " + s + " is not a valid attribute name!");
            }
        }

        /*Set<String> attributeSet = new HashSet<>(Set.of(attributes));
        attributeSet.add("objectClass");
        attributes = attributeSet.toArray(new String[attributes.length + 1]);*/

        FilterBuilder selectorEqual = selectorAttr == null ? null : equal(selectorAttr, escapeForSearch(selectorVal));
        FilterBuilder classEqualVar = equal("objectclass", classname);
        FilterBuilder tmpFilter = selectorAttr == null ? classEqualVar : and(selectorEqual, classEqualVar);

        return searchMultiInsecure(baseDn, tmpFilter.toString(),
                SearchScope.SUBTREE, attributes);
    }

    public List<Map<String, String>> searchChildren(String baseDn, String classname,
            String selectorAttr, String selectorVal, String... attributes)
            throws IOException, LdapException {
        List<Map<String, String>> retList = new ArrayList<>();

        List<Map<String, List<String>>> tmpRetMulti =
                searchChildrenMulti(baseDn, classname, selectorAttr, selectorVal, attributes);

        for (Map<String, List<String>> e1 : tmpRetMulti) {
            Map<String, String> tmpSet = new HashMap<>();
            for (Map.Entry<String, List<String>> e2 : e1.entrySet()) {
                if (e2.getValue().size() > 1) {
                    System.out.println(
                            "WARNING: Map result has more than one value, use searchChildrenMulti instead!");
                }
                tmpSet.put(e2.getKey(), e2.getValue().get(0));
            }
            retList.add(tmpSet);
        }
        return retList;
    }
    /*
    public List<Map<String, String>> searchChildrenUsafe(String baseDn, String filter, String... attributes)
            throws IOException, LdapException {
        List<Map<String, String>> retList = new ArrayList<>();

        List<Map<String, List<String>>> tmpRetMulti =
                searchMultiInsecure(baseDn, filter, SearchScope.ONELEVEL, attributes);

        for (Map<String, List<String>> e1 : tmpRetMulti) {
            Map<String, String> tmpSet = new HashMap<>();
            for (Map.Entry<String, List<String>> e2 : e1.entrySet()) {
                if (e2.getValue().size() > 1) {
                    System.out.println(
                            "WARNING: Map result has more than one value, use searchChildrenMulti instead!");
                }
                tmpSet.put(e2.getKey(), e2.getValue().get(0));
            }
            retList.add(tmpSet);
        }
        return retList;
    }
    **/
    /// Next 2 Functions taken from:
    /// https://stackoverflow.com/questions/31309673/parse-ldap-filter-to-escape-special-characters
    /// Author: Chris Janicki ( https://stackoverflow.com/users/939742/chris-janicki )
    /**
     * Filter components need to escape special chars.
     * Note that each piece of the filter needs to be escaped,
     * not the whole filter expression, for example:
     * "(&(cn="+ esc("Admins") +")(member="+ esc("CN=Doe\\, Jöhn,OU=ImPeople,DC=ds,DC=augur,DC=com") +"))"
     *
     * @see Oracle Directory Server Enterprise Edition 11g Reference doc
     * @see <a href="http://docs.oracle.com/cd/E29127_01/doc.111170/e28969/ds-ldif-search-filters.htm#gdxoy">Oracle Doc</a>
     * @param s A String field within the search expression
     * @return The escaped string, safe for use in the search expression.
     */
    protected static String escapeForSearch(String s) {
        if (s == null) {
            return "";
        }
        StringBuilder sb = new StringBuilder(s.length());
        for (byte c : s.getBytes(StandardCharsets.UTF_8)) {
            if (c == '\\') {
                sb.append("\\5c");
            } else if (c == '*') {
                sb.append("\\2a");
            } else if (c == '(') {
                sb.append("\\28");
            } else if (c == ')') {
                sb.append("\\29");
            } else if (c == 0) {
                sb.append("\\00");
            } else if ((c & 0xff) > 127) {
                sb.append("\\").append(to2CharHexString((c & 0xff)));
            } else { // UTF-8's non-7-bit characters, e.g. é, á, etc...
                sb.append((char) c);
            }
        }
        return sb.toString();
    }

    protected static String to2CharHexString(int i) {
        String s = Integer.toHexString(i & 0xff);
        if (s.length() == 1) {
            return "0" + s;
        }
        return s;
    }
}
