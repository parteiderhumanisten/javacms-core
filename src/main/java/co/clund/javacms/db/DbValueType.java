package co.clund.javacms.db;

public enum DbValueType {
    STRING, BLOB, INTEGER, REAL, TIMESTAMP;

    @Override
    public String toString() {
        if (this == STRING) {
            return "TEXT(65535)";
        } else if (this == TIMESTAMP) {
            return "LONG";
        }
        return super.toString();
    }
}
