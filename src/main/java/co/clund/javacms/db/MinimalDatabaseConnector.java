package co.clund.javacms.db;

import java.sql.*;
import java.util.*;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.stream.Collectors;

import co.clund.javacms.util.log.LoggingUtil;
import org.json.JSONObject;

public class MinimalDatabaseConnector {

    protected final java.util.logging.Logger logger = LoggingUtil.getLogger(MinimalDatabaseConnector.class);

    protected final String dbPath;
    protected final String dbUser;
    protected final String dbPassword;
    public final String dbPrefix;

    public MinimalDatabaseConnector(JSONObject dbConfig) {
        dbPath = dbConfig.getString("path");
        dbUser = dbConfig.getString("username");
        dbPassword = dbConfig.getString("password");

        if (dbConfig.has("dbPrefix")) {
            dbPrefix = dbConfig.getString("dbPrefix") + "_";
        } else {
            dbPrefix = "";
        }
    }

    public MinimalDatabaseConnector(String dbPath, String dbUser, String dbPassword, String prefix) {
        this.dbPath = dbPath;
        this.dbUser = dbUser;
        this.dbPassword = dbPassword;

        if (prefix != null) {
            this.dbPrefix = prefix;
        } else {
            this.dbPrefix = "";
        }
    }

    private Connection openConnection() throws SQLException {
        return DriverManager.getConnection(dbPath, dbUser, dbPassword);
    }

    public void executeUpdatePreparedQuery(List<DbValue> values, String preparedStatementString) {
        try (Connection con = openConnection(); PreparedStatement stmt
                = con.prepareStatement(preparedStatementString)) {

            addParametersToStatement(values, stmt, con);

            logger.log(Level.INFO, stmt.toString());

            stmt.executeUpdate();
            // con.commit();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void insert(String dbName, List<String> columns, List<DbValue> values) {
        String query = "INSERT INTO " + dbPrefix + dbName + " ("
                + generateCommaSeperatedList(columns)
                + ") VALUES ("
                + generateCommaSeperatedQuestionMarks(values.size())
                + ");";
        executeUpdatePreparedQuery(values, query);
    }

    public List<Map<String, DbValue>> select(String dbName, Map<String, DbValueType> returnTypes) {
        return select(dbName, List.of(), List.of(), returnTypes, null);
    }

    public List<Map<String, DbValue>> select(String dbName, Map<String, DbValueType> returnTypes, String append) {
        return select(dbName, List.of(), List.of(), returnTypes, append);
    }

    public List<Map<String, DbValue>> select(String dbName, String columns, DbValue selector,
                                             Map<String, DbValueType> returnTypes) {
        return select(dbName, List.of(columns), List.of(selector), returnTypes);
    }

    public List<Map<String, DbValue>> select(String dbName, List<String> columns, List<DbValue> selector,
                                             Map<String, DbValueType> returnTypes) {
        return select(dbName, columns, selector, returnTypes, "");
    }

    public List<Map<String, DbValue>> select(String dbName, String columns, DbValue selector,
                                             Map<String, DbValueType> returnTypes, String append) {
        return select(dbName, List.of(columns), List.of(selector), returnTypes, append);
    }

    public List<Map<String, DbValue>> select(String dbName, List<String> columns, List<DbValue> selector,
                                             Map<String, DbValueType> returnTypes, String append) {
        StringBuilder sb = new StringBuilder();

        sb.append("SELECT * FROM ").append(dbPrefix).append(dbName).append(" WHERE ");

        for (String c : columns) {
            sb.append(c).append(" = ? AND ");
        }

        sb.append(" TRUE ").append(append == null ? "" : append).append(" ;");

        List<Map<String, DbValue>> retList = new ArrayList<>();

        try (Connection con = openConnection(); PreparedStatement stmt = con.prepareStatement(sb.toString())) {

            addParametersToStatement(selector, stmt, con);

            logger.log(Level.INFO, stmt.toString());

            try (ResultSet rs = stmt.executeQuery()) {
                ResultSetMetaData rsmd = rs.getMetaData();

                Map<String, Integer> columnIdMap = new HashMap<>();

                for (int j = 1; j < (rsmd.getColumnCount() + 1); j++) {
                    columnIdMap.put(rsmd.getColumnName(j), j);
                }

                while (rs.next()) {
                    Map<String, DbValue> tmpMap = new HashMap<>();

                    for (Entry<String, DbValueType> e : returnTypes.entrySet()) {

                        int columnNumber = columnIdMap.get(e.getKey());

                        switch (e.getValue()) {
                            case BLOB:
                                tmpMap.put(e.getKey(), new DbValue(rs.getBlob(columnNumber)));
                                break;

                            case STRING:
                                tmpMap.put(e.getKey(), new DbValue(rs.getString(columnNumber)));
                                break;

                            case INTEGER:
                                tmpMap.put(e.getKey(), new DbValue(rs.getInt(columnNumber)));
                                break;

                            case REAL:
                                tmpMap.put(e.getKey(), new DbValue(rs.getDouble(columnNumber)));
                                break;

                            case TIMESTAMP:
                                final long timestampMillis = rs.getLong(columnNumber);
                                final DbValue dbValue = timestampMillis > 0
                                        ? new DbValue(new Timestamp(timestampMillis))
                                        : new DbValue((Timestamp) null);
                                tmpMap.put(e.getKey(), dbValue);
                                break;

                            default:
                                throw new RuntimeException("unknown value type in "
                                        + "DatabaseConnector::executePreparedQuery");

                        }
                    }
                    retList.add(tmpMap);

                }

            } catch (Exception e) {
                throw new RuntimeException(e);
            }

        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return retList;
    }

    public ResultSet executeQuery(String sql) {
        try (Connection con = openConnection(); PreparedStatement stmt = con.prepareStatement(sql)) {
            logger.log(Level.INFO, stmt.toString());
            return stmt.executeQuery();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void executeUpdate(String sql) {
        try (Connection con = openConnection(); PreparedStatement stmt = con.prepareStatement(sql)) {
            logger.log(Level.INFO, stmt.toString());
            stmt.executeUpdate();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static void addParametersToStatement(List<DbValue> selector, PreparedStatement stmt, Connection con)
            throws SQLException {
        int i = 1;
        for (DbValue v : selector) {
            switch (v.getDbValueType()) {
                case BLOB:
                    stmt.setBlob(i, v.getBlob(con));
                    break;

                case STRING:
                    stmt.setString(i, v.getString());
                    break;

                case INTEGER:
                    stmt.setInt(i, v.getInt());
                    break;

                case REAL:
                    stmt.setDouble(i, v.getDouble());
                    break;

                case TIMESTAMP:
                    final long time = v.getTimestamp() == null ? -1 : v.getTimestamp().getTime();
                    stmt.setLong(i, time);
                    break;

                default:
                    throw new RuntimeException("unknown value type in DatabaseConnector::executePreparedQuery");
            }
            i++;
        }
    }

    private static String generateCommaSeperatedList(List<String> columns) {
        StringJoiner sj = new StringJoiner(",");

        for (String c : columns) {
            sj.add(c);
        }

        return sj.toString();
    }

    private static String generateCommaSeperatedQuestionMarks(int size) {
        StringJoiner sj = new StringJoiner(",");
        for (int i = 0; i < size; i++) {
            sj.add("?");
        }
        return sj.toString();
    }

    public void updateValue(String tableName, String valueColumns, DbValue valueValues, String selectorColumns,
                            DbValue selectorIds) {
        updateValue(tableName, List.of(valueColumns), List.of(valueValues), List.of(selectorColumns),
                List.of(selectorIds));
    }

    public void updateValue(String tableName, List<String> valueColumns, List<DbValue> valueValues,
                            String selectorColumns, DbValue selectorIds) {
        updateValue(tableName, valueColumns, valueValues, List.of(selectorColumns), List.of(selectorIds));
    }

    public void updateValue(String tableName, String valueColumn, DbValue valueValue) {
        updateValue(tableName, List.of(valueColumn), List.of(valueValue), new ArrayList<String>(), new ArrayList<DbValue>());
    }

    public void updateValue(String tableName, List<String> valueColumns, List<DbValue> valueValues,
                            List<String> selectorColumns, List<DbValue> selectorIds) {

        String condition = "";
        if (!selectorColumns.isEmpty()) {
            condition = " WHERE "
                  + selectorColumns.stream()
                   .map(c -> c.concat(" = ?"))
                   .collect(Collectors.joining(" AND "));
        }
        String query = "UPDATE " + dbPrefix + tableName + " SET "
                   + valueColumns.stream()
                   .map(c -> c.concat(" = ?"))
                   .collect(Collectors.joining(" , "))
                   + condition
                   + ";";

        try (Connection con = openConnection(); PreparedStatement stmt = con.prepareStatement(query)) {
            List<DbValue> allParams = new ArrayList<>();
            allParams.addAll(valueValues);
            allParams.addAll(selectorIds);

            addParametersToStatement(allParams, stmt, con);

            logger.log(Level.INFO, stmt.toString());

            stmt.executeUpdate();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    public void deleteFrom(String tableName, String selectorColumn, DbValue selectorIds) {
        deleteFrom(tableName, List.of(selectorColumn), List.of(selectorIds));
    }

    public void deleteFrom(String tableName, List<String> selectorColumns, List<DbValue> selectorIds) {

        StringBuilder sb = new StringBuilder();

        sb.append("DELETE FROM ").append(dbPrefix).append(tableName).append(" WHERE ");

        for (String c : selectorColumns) {
            sb.append(c).append(" = ? AND ");
        }

        sb.append(" TRUE;");

        String query = sb.toString();

        try (Connection con = openConnection(); PreparedStatement stmt = con.prepareStatement(query)) {
            addParametersToStatement(selectorIds, stmt, con);

            logger.log(Level.INFO, stmt.toString());

            stmt.executeUpdate();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    public void addColumn(String dbTableName, String columnName, DbValueType type) {
        StringBuilder query = new StringBuilder("ALTER TABLE " + dbPrefix + dbTableName + " ADD " + columnName + " ");

        switch (type) {

            case STRING:
                query.append("TEXT(65535)");
                break;

            case BLOB:
                query.append("BLOB");
                break;

            case INTEGER:
                query.append("INTEGER");
                break;

            case REAL:
                query.append("REAL");
                break;

            case TIMESTAMP:
                query.append("LONG");
                break;

            default:
                throw new RuntimeException("unknown value type in AbstractDbTable::createTable");

        }
        query.append(";");

        // ALTER TABLE table_name
        // ADD
        // column_name_1 data_type_1

        try (Connection con = openConnection(); PreparedStatement stmt = con.prepareStatement(query.toString())) {
            logger.log(Level.INFO, stmt.toString());

            stmt.executeUpdate();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }
}
