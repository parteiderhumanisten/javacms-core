package co.clund.javacms.util;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * An ordered combination of two values closely associated with each other.
 *
 * <p>Generally, a Pair is immutable, although no guarantees are made for the
 * mutability of its values. The values, for simplicity, are called "left" and
 * "right".
 *
 * @param <L> The type of the left value.
 * @param <R> The type of the right value.
 */
public class Pair<L, R> {

    /**
     * The left value.
     */
    private final L left;
    /**
     * The right value.
     */
    private final R right;

    /**
     * Creates a pair of two values.
     *
     * @param left  The left value.
     * @param right The right value.
     * @param <L>   The left value's type.
     * @param <R>   The right value's type.
     * @return A pair of the two values.
     */
    public static <L, R> Pair<L, R> of(L left, R right) {
        return new Pair<>(left, right);
    }

    /**
     * Creates a pair with the entrys key being the left value and the entrys value
     * being the right.
     *
     * @param entry The entry.
     * @param <L>   The type of the left value / entry key.
     * @param <R>   The type of the right value / entry value.
     * @return A pair of the two values.
     */
    public static <L, R> Pair<L, R> of(Map.Entry<L, R> entry) {
        return new Pair<>(entry.getKey(), entry.getValue());
    }

    /**
     * Creates a pair of the first two values of any iterable structure.
     *
     * <p>The first value will be left, the second will be right.
     *
     * @param elements The iterator source.
     * @param <L>      The type of the pairs values.
     * @return A pair of the first two values.
     * @throws IllegalArgumentException If the element source has less than two elements.
     */
    public static <L> Pair<L, L> of(Iterable<L> elements) {
        Iterator<L> iterator = elements.iterator();
        if (!iterator.hasNext()) {
            throw new IllegalArgumentException("Must have at least two elements");
        }
        L left = iterator.next();
        if (!iterator.hasNext()) {
            throw new IllegalArgumentException("Must have at least two elements");
        }
        return new Pair<>(left, iterator.next());
    }

    /**
     * Creates a pair of the first two values of any iterable structure.
     *
     * <p>The first value will be left, the second will be right.
     *
     * @param elements The iterator source.
     * @param <L>      The type of the pairs values.
     * @return A pair of the first two values.
     * @throws IllegalArgumentException If the array has less than two elements.
     */
    public static <L> Pair<L, L> of(L[] elements) {
        if (elements.length < 2) {
            throw new IllegalArgumentException("Must have at least two elements");
        }
        return new Pair<>(elements[0], elements[1]);
    }

    /**
     * Creates a pair of two values.
     *
     * @param left  The left value.
     * @param right The right value.
     * @throws NullPointerException If any one of the values is {@code null}.
     */
    public Pair(L left, R right) {
        this.left = Objects.requireNonNull(left);
        this.right = Objects.requireNonNull(right);
    }

    /**
     * Gets the left value.
     *
     * @return The left value.
     */
    public L left() {
        return left;
    }

    /**
     * Gets the right value.
     *
     * @return The right value.
     */
    public R right() {
        return right;
    }

    /**
     * Returns a pair with the values swapped.
     *
     * @return The inverted pair.
     */
    public Pair<R, L> swap() {
        return new Pair<>(right, left);
    }

    /**
     * Stringify the pair.
     *
     * <p>Both values will be converted to strings. If both already
     * are Strings, no new instance is created.
     *
     * @return A pair with string representations of this pairs contents.
     */
    @SuppressWarnings("unchecked")
    public Pair<String, String> asStrings() {
        if (left instanceof String && right instanceof String) {
            // This is *not* an unchecked cast but the compiler does not realize that
            return (Pair<String, String>) this;
        }
        return new Pair<>(left.toString(), right.toString());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Pair<?, ?> pair = (Pair<?, ?>) o;
        return left.equals(pair.left) && right.equals(pair.right);
    }

    @Override
    public int hashCode() {
        return Objects.hash(left, right);
    }

    @Override
    public String toString() {
        return "Pair(" + left.toString() + ", " + right.toString() + ")";
    }

    /**
     * Extract the left values from a collection of pairs.
     *
     * @param list The pairs.
     * @param <L>  The type of the left values.
     * @param <R>  The type of the right values.
     * @return A list of all left values.
     */
    public static <L, R> List<L> leftOnly(Iterable<Pair<L, R>> list) {
        final Iterator<Pair<L, R>> iter = list.iterator();
        final List<L> lefts = new ArrayList<>();
        while (iter.hasNext()) {
            lefts.add(iter.next().left);
        }
        return lefts;
    }

    /**
     * Extract the right values from a collection of pairs.
     *
     * @param list The pairs.
     * @param <L>  The type of the left values.
     * @param <R>  The type of the right values.
     * @return A list of all right values.
     */
    public static <L, R> List<R> rightOnly(Iterable<Pair<L, R>> list) {
        final Iterator<Pair<L, R>> iter = list.iterator();
        final List<R> rights = new ArrayList<>();
        while (iter.hasNext()) {
            rights.add(iter.next().right);
        }
        return rights;
    }


    public static <L, R, T> List<Pair<L, R>> extractPairs(Collection<T> source,
                                                          Function<T, L> leftExtractor,
                                                          Function<T, R> rightExtractor) {
        return source.stream()
                .map(entry -> Pair.of(leftExtractor.apply(entry), rightExtractor.apply(entry)))
                .collect(Collectors.toList());
    }

    /**
     * Compare pairs by their left value only.
     *
     * @param <L> The left type. Must be comparable.
     * @param <R> The right type.
     * @return A comparator comparing the left values.
     */
    public static <L extends Comparable<? super L>, R> Comparator<Pair<L, R>> byLeftComparator() {
        return Comparator.comparing(Pair::left);
    }

    /**
     * Compare pairs by their right value only.
     *
     * @param <L> The left type.
     * @param <R> The right type. Must be comparable.
     * @return A comparator comparing the right values.
     */
    public static <L, R extends Comparable<? super R>> Comparator<Pair<L, R>> byRightComparator() {
        return Comparator.comparing(Pair::right);
    }
}
