package co.clund.javacms.util;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.Base64;

public class HashUtil {

    public static String getSimpleSha256Hash(String data) {
        try {
            MessageDigest crypt = MessageDigest.getInstance("SHA-256");
            crypt.reset();
            crypt.update(data.getBytes());
            byte[] hash = crypt.digest();

            return Base64.getEncoder().encodeToString(hash);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static String getSha256Hash(String secretToken, String target) {
        try {
            MessageDigest crypt = MessageDigest.getInstance("SHA-256");
            crypt.reset();
            crypt.update(secretToken.getBytes());
            crypt.update(target.getBytes());
            byte[] hash = crypt.digest();

            return Base64.getEncoder().encodeToString(hash);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static final int HASH_BIT_SIZE = 64 * 8; // 512 bits
    private static final int SALT_BYTE_SIZE = 16; // 64 would be more ideal;
    private static final int PBKDF2_ITERATIONS = 10000;

    public static String getLdapPBKDF2WithHmacSHA512ForLdap(String pwd)
            throws NoSuchAlgorithmException, InvalidKeySpecException {
        SecureRandom random = new SecureRandom();
        byte[] salt = new byte[SALT_BYTE_SIZE];
        random.nextBytes(salt);

        PBEKeySpec spec = new PBEKeySpec(pwd.toCharArray(), salt, PBKDF2_ITERATIONS, HASH_BIT_SIZE);
        SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA512");
        byte[] hash = skf.generateSecret(spec).getEncoded();

        return "{PBKDF2-SHA512}" + PBKDF2_ITERATIONS + "$" + ldapBase64encode(salt) + "$" + ldapBase64encode(hash);
    }

    private static String ldapBase64encode(byte[] data) {
        return Base64.getEncoder().encodeToString(data).replace("+", ".").replace("=", "");
    }

    public static String getSHA1(String input) {
        MessageDigest messageDigest;
        try {
            messageDigest = MessageDigest.getInstance("SHA1");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
        byte[] result = messageDigest.digest(input.getBytes());
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < result.length; i++) {
            sb.append(Integer.toString((result[i] & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
    }
}
