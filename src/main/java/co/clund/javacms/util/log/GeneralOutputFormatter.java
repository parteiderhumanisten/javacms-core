package co.clund.javacms.util.log;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

public class GeneralOutputFormatter extends Formatter {

    @Override
    public String format(LogRecord rec) {
        StringWriter sb = new StringWriter();

        sb.append("[ ").append(String.valueOf(rec.getLevel())).append(" @ ");
        sb.append(calcDate(rec.getMillis())).append(" @ ");
        sb.append(rec.getSourceClassName()).append("::").append(rec.getSourceMethodName()).append(" ] ");
        sb.append(formatMessage(rec)).append(String.valueOf('\n'));
        if (rec.getThrown() != null) {
            rec.getThrown().printStackTrace(new PrintWriter(sb));
        }
        return sb.toString();
    }

    private static String calcDate(long millisecs) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date resultdate = new Date(millisecs);
        return dateFormat.format(resultdate);
    }

    @Override
    public String getHead(Handler h) {
        return "Log starting @ " + (new Date()) + "\n";
    }

    @Override
    public String getTail(Handler h) {
        return "Log ended @ " + (new Date()) + "\n";
    }
}
