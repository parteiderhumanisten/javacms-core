package co.clund.javacms.util.log;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.*;

public class LoggingUtil {

    public static final String LOGGING_DESTINATION_DIR = "log/";

    private static final Map<String, Logger> loggerList = new HashMap<>();

    public static Logger getLogger(Class<? extends Object> clazz) {
        return getLogger(clazz, Level.ALL);
    }

    public static synchronized Logger getLogger(Class<? extends Object> clazz, Level level) {
        String name = clazz.getCanonicalName();

        if (loggerList.containsKey(name)) {
            return loggerList.get(name);
        }

        createLogDir();

        try {
            Logger l = Logger.getLogger(name, null);
            l.setUseParentHandlers(false);

            loggerList.put(name, l);

            l.setLevel(level);
            FileHandler fileHandler = new FileHandler(LOGGING_DESTINATION_DIR + name + ".log", true);

            // create a TXT formatter
            ModuleOutputFormatter formatter = new ModuleOutputFormatter();
            fileHandler.setFormatter(formatter);
            l.addHandler(fileHandler);

            ConsoleHandler hCon = new ConsoleHandler();
            hCon.setFormatter(new GeneralOutputFormatter());
            l.addHandler(hCon);

            return l;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static void createLogDir() {
        File f = new File(LOGGING_DESTINATION_DIR);

        if (f.isFile()) {
            throw new RuntimeException(LOGGING_DESTINATION_DIR + " should be a directory and not a file!");
        }

        if (!f.isDirectory()) {
            f.mkdir();
        }
    }
}
