package co.clund.javacms.module;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import co.clund.javacms.ConfigContext;
import co.clund.javacms.util.log.LoggingUtil;

import org.reflections.Reflections;

public class ModuleUtil {

    private static final Set<Class<? extends AbstractModule>> moduleList = loadModuleClasses();

    private static Set<Class<? extends AbstractModule>> loadModuleClasses() {
        return loadModuleClassesFromPackage("de.diehumanisten.portal.module");
    }

    public static Set<Class<? extends AbstractModule>>
            loadModuleClassesFromPackage(String packagePath) {
        Reflections reflections = new Reflections(packagePath);

        Set<Class<? extends AbstractModule>> retSet =
                reflections.getSubTypesOf(AbstractModule.class);

        for (Class<? extends AbstractModule> c : retSet) {
            if (Modifier.isAbstract(c.getModifiers())) {
                retSet.remove(c);
            }
        }

        return retSet;
    }

    public static AbstractModule createModuleFromClass(AbstractModule parentModule,
            ConfigContext context, Class<? extends AbstractModule> c) {
        Constructor<? extends AbstractModule> cons;
        Logger logger = LoggingUtil.getLogger(ModuleUtil.class);
        try {
            cons = c.getConstructor(AbstractModule.class, ConfigContext.class);
            AbstractModule m = cons.newInstance(parentModule, context);
            logger.log(Level.INFO, "Adding module \"" + m.getModuleName() + "\" from class \""
                    + m.toString() + "\"");
            return m;
        } catch (Exception e) {
            logger.log(Level.SEVERE, "error while instanciating module: " + c.getName());
            throw new RuntimeException(e);
        }
    }

    public static Set<AbstractModule> createSubModuleClasses(AbstractModule parentModule,
            String packagePath, ConfigContext context) {
        if (context == null) {
            throw new RuntimeException("context is null");
        }

        Set<Class<? extends AbstractModule>> classes = loadModuleClassesFromPackage(packagePath);

        Set<AbstractModule> retSet = new HashSet<>();

        for (Class<? extends AbstractModule> c : classes) {
            retSet.add(createModuleFromClass(parentModule, context, c));
        }

        return retSet;
    }

    public static Set<Class<? extends AbstractModule>> getModuleClasses() {
        return moduleList;
    }

}
