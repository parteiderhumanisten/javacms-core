package co.clund.javacms.module;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.logging.Level;
import java.util.logging.Logger;

import co.clund.javacms.html.HtmlPostForm;
import co.clund.javacms.util.log.LoggingUtil;
import co.clund.javacms.UserSession;
import co.clund.javacms.db.MinimalDatabaseConnector;
import co.clund.javacms.module.FunctionResult.Status;
import co.clund.javacms.util.HashUtil;
import co.clund.javacms.util.TriFunction;
import org.apache.http.client.utils.URIBuilder;

public abstract class AbstractModule {

    protected static final String MESSAGE_GET_VAR = "message";

    protected final String name;
    protected final String parentModule;
    protected final MinimalDatabaseConnector dbCon;
    protected final Logger logger = LoggingUtil.getLogger(AbstractModule.class);

    public AbstractModule(String parentModule, String name, MinimalDatabaseConnector dbCon) {
        this.name = name;
        this.parentModule = parentModule;
        this.dbCon = dbCon;
        functionMap = loadFunctions();
        multipartFunctionMap = loadMultipartFunctions();
        subModuleMap = loadSubModules();
    }

    public String getModuleName() {
        return name;
    }

    public String getModulePath() {
        return parentModule + "/" + name;
    }

    public String getParentModulePath() {
        return parentModule;
    }

    public final byte[] invoke(String method, final HttpServletResponse response, UserSession s, String modulePath,
                               Map<String, String[]> parameters, Map<String, Part> parts) {
        try {
            if ((modulePath == null) || modulePath.equals("")) {
                return invokePlain(s, parameters);
            }

            if (modulePath.startsWith("/")) {

                String moduleRelativePath = modulePath.substring(1);

                int subModuleNameLength = -1;

                if (moduleRelativePath.contains("/")) {
                    subModuleNameLength = moduleRelativePath.indexOf("/");
                }
                if (moduleRelativePath.contains(".")) {
                    subModuleNameLength = moduleRelativePath.indexOf(".");
                }

                if (subModuleNameLength < 0) {
                    subModuleNameLength = moduleRelativePath.length();
                }

                String subModuleName = moduleRelativePath.substring(0, subModuleNameLength);

                String subModulePath = moduleRelativePath.substring(subModuleName.length());

                if (subModuleMap.containsKey(subModuleName)) {
                    AbstractModule invokedModule = subModuleMap.get(subModuleName);

                    return invokedModule.invoke(method, response, s, subModulePath, parameters, parts);
                }
                return ("Not found:<br>" + subModuleName + "<br><a href='/'>return to main page</a>").getBytes();
            }

            if (!method.equals("POST")) {
                AbstractModule invokedModule = dbCon.getListener().getReqHandler().getModuleMap().get(_400.LOCATION);
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                return invokedModule.invoke("GET", response, s, "", new HashMap<>(), null);
            }

            String functionName = modulePath.substring(1); // remove the "." in front of the path the get the function
            // name

            FunctionResult r;
            if (parts == null) {
                if (!HashUtil.getSha256Hash(s.getCsrfSecret(), functionName)
                        .equals(parameters.get(HtmlPostForm.CSRF_TOKEN_KEY)[0])) {
                    AbstractModule invokedModule = dbCon.getListener().getReqHandler().getModuleMap()
                            .get(_csrf_error.LOCATION);
                    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    return invokedModule.invoke("GET", response, s, "", new HashMap<>(), null);
                }

                BiFunction<UserSession, Map<String, String[]>, FunctionResult> f = functionMap.get(functionName);

                if (f == null) {
                    AbstractModule invokedModule = dbCon.getListener().getReqHandler().getModuleMap()
                            .get(_404.LOCATION);
                    response.setStatus(HttpServletResponse.SC_NOT_FOUND);
                    return invokedModule.invoke("GET", response, s, "", new HashMap<>(), null);
                }

                r = f.apply(s, parameters);
            } else {
                if (!HashUtil.getSha256Hash(s.getCsrfSecret(), functionName)
                        .equals(new String(parts.get(HtmlPostForm.CSRF_TOKEN_KEY).getInputStream().readAllBytes()))) {
                    AbstractModule invokedModule = dbCon.getListener().getReqHandler().getModuleMap()
                            .get(_csrf_error.LOCATION);
                    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    return invokedModule.invoke("GET", response, s, "", Map.of(), null);
                }

                TriFunction<UserSession, Map<String, String[]>, Map<String, Part>, FunctionResult> f
                        = multipartFunctionMap.get(functionName);

                if (f == null) {
                    return FunctionResult.Status.NOT_FOUND.name().getBytes();
                }

                r = f.apply(s, parameters, parts);
            }

            if (r.getStatus() == Status.DATA_RESPONSE) {
                response.setHeader("Content-Transfer-Encoding", "binary");
                response.setHeader("Content-Disposition", "attachment; filename=" + r.getFilename());
                response.setHeader("Content-Type", r.getMimeType());
                return r.getData();
            }

            URIBuilder b = r.getBuilder();
            if (r.getStatus() != FunctionResult.Status.NONE) {
                b.addParameter(MESSAGE_GET_VAR, r.getMessage());
            }

            if (response != null) {
                response.addHeader("Location", b.build().toString());
                response.setStatus(HttpServletResponse.SC_FOUND);
            }

            return r.getStatus().name().getBytes();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "error while invoking", e);
            return (e.getMessage() + "").getBytes();
        }
    }

    protected abstract byte[] invokePlain(UserSession s, Map<String, String[]> parameters);

    protected final Map<String, BiFunction<UserSession, Map<String, String[]>, FunctionResult>> functionMap;

    protected abstract Map<String, BiFunction<UserSession, Map<String, String[]>, FunctionResult>> loadFunctions();

    protected final Map<String, TriFunction<UserSession, Map<String, String[]>, Map<String, Part>, FunctionResult>>
            multipartFunctionMap;

    @SuppressWarnings("static-method")
    protected Map<String, TriFunction<UserSession, Map<String, String[]>, Map<String, Part>,
            FunctionResult>> loadMultipartFunctions() {
        return new HashMap<>();
    }

    protected final Map<String, AbstractModule> subModuleMap;

    @SuppressWarnings("static-method")
    public Map<String, AbstractModule> loadSubModules() {
        return new HashMap<>();
    }
}
