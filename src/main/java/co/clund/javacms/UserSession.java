package co.clund.javacms;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import co.clund.javacms.db.MinimalDatabaseConnector;
import co.clund.javacms.util.RandomUtil;

public abstract class UserSession<UserClass extends User> {

    protected final HttpSession session;
    protected final UserClass thisUser;
    protected final MinimalDatabaseConnector dbCon;
    protected final String clientIp;

    UserSession(MinimalDatabaseConnector dbCon, HttpServletRequest request) {
        session = request.getSession();

        this.dbCon = dbCon;

        String tmpClientIp = request.getRemoteAddr();
        if (tmpClientIp.equals("127.0.0.1")) {
            if (request.getHeader("X-Real-IP") != null) {
                tmpClientIp = request.getHeader("X-Real-IP");
            } else if (request.getHeader("X-Forwarded-For") != null) {
                tmpClientIp = request.getHeader("X-Forwarded-For");
            }
        }

        this.clientIp = tmpClientIp;

        if ((session.getAttribute("userId") == null)) {
            thisUser = null;
        } else {
            thisUser = getUserById((Integer) session.getAttribute("userId"));
        }
        if (session.getAttribute("csrfToken") == null) {
            session.setAttribute("csrfToken", RandomUtil.getRandomString());
        }
    }

    public UserClass getThisUser() {
        return thisUser;
    }

    public void setUser(UserClass u) {
        if (u == null) {
            session.removeAttribute("userId");
            return;
        }
        session.setAttribute("userId", Integer.valueOf(u.getUid()));
    }

    public HttpSession getSession() {
        return session;
    }

    public void clear() {
        session.invalidate();
    }

    public MinimalDatabaseConnector getDbCon() {
        return dbCon;
    }

    public String getClientIp() {
        return clientIp;
    }

    public String getCsrfSecret() {
        return (String) session.getAttribute("csrfToken");
    }

    protected abstract UserClass getUserById(int id);
}
