package co.clund.javacms.html;

public class HtmlGetForm extends HtmlForm {

    public HtmlGetForm(String actionTarget, boolean multipart) {
        super(actionTarget, Method.GET, multipart);
    }

    public HtmlGetForm(String actionTarget, Direction direction, boolean multipart) {
        super(actionTarget, Method.GET, direction, multipart);
    }

    public HtmlGetForm(String actionTarget, Direction direction) {
        super(actionTarget, Method.GET, direction);
    }

    public HtmlGetForm(String actionTarget) {
        super(actionTarget, Method.GET);
    }

}
