package co.clund.javacms;

import java.lang.reflect.Constructor;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.json.JSONObject;

import co.clund.javacms.module.ModuleUtil;
import co.clund.javacms.module.AbstractModule;

public class JavaCmsApp<_ConfigContext extends ConfigContext> {

    private final Map<String, AbstractModule> moduleMap = new HashMap<>();

    public final _ConfigContext configContext;

    @SuppressWarnings("unchecked")
    public JavaCmsApp(JSONObject config) throws Exception {
        Constructor<? extends ConfigContext> cons = null;
        final Type type = JavaCmsApp.class.getGenericInterfaces()[0];
        final Class<? extends ConfigContext> clazz = (Class<? extends ConfigContext>) type.getClass();
        cons = clazz.getConstructor(JSONObject.class);

        configContext = (_ConfigContext) cons.newInstance(config);

        // TODO Auto-generated constructor stub
    }

    protected void loadModulesFromPackage(String packageName) {
        Set<Class<? extends AbstractModule>> classes = ModuleUtil.loadModuleClassesFromPackage(packageName);

        for (Class<? extends AbstractModule> c : classes)
            loadModule(c);
    }

    protected void loadModule(Class<? extends AbstractModule> clazz) {
        AbstractModule module = ModuleUtil.createModuleFromClass(null, /* TODO: change */ null, clazz);
        moduleMap.put(module.getModuleName(), module);
    }
}
